library("TDA")
library(lubridate)
options(stringsAsFactors = FALSE)
#set seed to 711 to reproduce results
set.seed(711)

#input <- read.csv("phase1_test.csv", sep = ",", header = TRUE)
#input <- read.csv("phase1_train.csv", sep = ",", header = TRUE)
input <- read.csv("phase2_test.csv", sep = ",", header = T)
#input <- read.csv("phase2_train.csv", sep = ",", header = TRUE)
input$timestamp[is.na(input$timestamp)] <- input[1, "timestamp"]
input[is.na(input)] <- 0
#Convert timestamp column and extract week number and weekday  n
input$timestamp <- as.POSIXct(input$timestamp, format = "%Y-%m-%d %H:%M:%S")
input$timeofday <- strftime(input$timestamp,format = "%H:%M:%S")
input$week <-  format(input$timestamp, format = '%U')
input$week <- as.numeric(input$week)
input$dayofweek <- wday(input$timestamp)
input$window <- 1:nrow(input) %% 288
input$window[input$window == 0] <- 288
summary(input)

#identify sensors
sensors <- names(input)[grepl("S", names(input))]
#Select fewer sensors
#sensors <- sensors[c(5,10,11)]

#window size in 5 min increments
w_s <- 12
w_N <- 289-w_s

#Hyperparameters:  Number of bags and number of samples per bag
b_N = 30
b_s = 30
#Scale of persistence diagrams
msc = 2000

#create a dataframe to store the feature vectors
N <- w_N*6*52*length(sensors)
feature_vectors <- data.frame(data.frame(matrix(data = NA,nrow = N,ncol = b_N + 4)))
colnames(feature_vectors) <- c("sensor", "week", "dayofweek", "window", paste("bag",seq(1,b_N),sep = ""))

#Store the info on which bags were used for each day of the week
Bag_Choices <- data.frame(matrix(data = NA, nrow = 6*b_N, ncol = (b_s+2)))
colnames(Bag_Choices)[1:2] <- c("dayofweek", "bag_number")

#Load incidents to record diagrams for these incidents
#incidents <- read.csv("TrainingIncidents.csv", header = TRUE, sep = ",")
#inc_bagdata <- data.frame(matrix(data = NA, ncol = w_s+2))
#colnames(inc_bagdata)[c(1,2)]= c("window", "bag")
#inc_modbagdata <- data.frame(matrix(data = NA, ncol = w_s+2))
#colnames(inc_modbagdata)[c(1,2)]= c("window", "bag")
#inc_sensor <- incidents$Station.ID[1]
#inc_week <- incidents$Week[1]
#inc_day <- incidents$Week.Day[1]
#inc_window <- seq(incidents$Window[1]-6,incidents$Window[1]+6)


t_0 <- Sys.time()
n <- 1
B <- matrix(data = NA, nrow = b_N, ncol = b_s)
  #build reference bags
  for (b_n in 1:b_N){
    bag <- sample(1:52, b_s, replace = FALSE)
    B[b_n, ] <- bag}
  #Bag_Choices[(1+(d-2)*30):((d-1)*30),3:(b_s+2)] <- B
  #Bag_Choices[30*(d-2)+1:30*(d-1),1] <- d
  #Bag_Choices[30*(d-2)+1:30*(d-1),3:b_s+2] <- 1:b_N
#Start looping through day, sensor, window...
for (d in 2:7){
  wks <- 52
  m <- n-1+wks
    for (w_n in 1:w_N){
      input_window <- input[input$window >= w_n & input$window < w_n+w_s & input$dayofweek == d, ]
      for (sensor in sensors){
        #build reference diagrams and store distances in a matrix FVs
        FVs <- matrix(data = NA, nrow = wks, ncol = b_N)
        for (b_n in 1:b_N){
          bag_data <- matrix(data = NA, nrow = b_s, ncol = w_s)
            for (i in 1:b_s){
            input_week <- na.omit(input_window[input_window$week == B[b_n,i], sensor])
            bag_data[i,] <- input_week
            }
           diagram_ref <- ripsDiag(bag_data, maxdimension = 0, maxscale = msc, library = "Dionysus")
           for (week in 1:52){
              modbag_data <- bag_data
              input_week <- na.omit(input_window[input_window$week == week, sensor])
              i_replace <- sample(1:b_s,1, replace = FALSE)
              modbag_data[i_replace,] <- input_week 
              diagram_week <- ripsDiag(modbag_data, maxdimension =0, maxscale = msc, library = "Dionysus")
              dist <- bottleneck(Diag1= diagram_ref[["diagram"]], Diag2= diagram_week[["diagram"]], dimension = 0)
              FVs[week, b_n] <- dist
              }}
              feature_vectors[n:m, 5:(4+b_N)] <- FVs
              feature_vectors[n:m, "sensor"] <- sensor
              feature_vectors[n:m, "week"] <- 1:52
              feature_vectors[n:m, "dayofweek"] <- d
              feature_vectors[n:m, "window"] <- w_n
              n <- n+wks
              m <- m + wks
                    }
      print(c(d,w_n, "done"))
    }
  }
t_f <- Sys.time()
total_time <- t_f - t_0
total_time

inc_bagdata1 <- na.omit(inc_bagdata)
inc_modbagdata1 <- na.omit(inc_modbagdata)

#write.table(x = feature_vectors, file = "fvs60m_phase1tst_5-21-20.csv", row.names = FALSE, col.names = TRUE, sep = ",")
write.table(x = feature_vectors, file = "fvs60m_phase2tst_6-17-20.csv", row.names = FALSE, col.names = TRUE, sep = ",")
#write.table(x = feature_vectors, file = "fvs60m_phase1_4-9-20.csv", row.names = FALSE, col.names = TRUE, sep = ",")
