Readme for randomized-persistance-landscapes 

Data Sets:

There are 4 data sets:  phase1_train.csv, phase1_test.csv, phase2_train.csv, phase2_test.csv

For each data set, there is a also a .csv file of incidents, with the time, location, and durration of each incident.

To perform the experiments described in "Detecting Traffic Incidents Using Persistence Diagrams," 5 scripts are required.

1. random_bagging.R - takes any of the four data sets an produces an array consisting of bottleneck distances (bnd)
	calculated using the TDA library.

2. fv_ranking.R - takes the array produced by random_bagging.R and adds rankings based on average bnd, standard deviation bnd,
	and median bnd.  Rankings are given in the form of a percentile.

3. Thresholds.R - takes csv files with incident data and the ranked bnd vectors to determine the highest anomaly score occuring
	near each reported incident.  Also determines minimum anomaly scores required for incident classification.

4. WriteMask.R - takes ranked bnd vectors, raw counts, and thresholds determined by Thresholds.R to classify time windows as
	incidents or non-incidents.  Output is in the form of a binary matrix of the same shape as the original counts.  1
	indicates an incident is predicted and 0 indicates no detected incident.

5.  MeasurePerformance.R - takes the original counts and binary mask produced by WriteMask.R to measure the performance of 
	the classification.  For each sensor, we are given a score for precision, recall, and F-score.  There is also an
	aggregate score which combines performance over all the sensors.

